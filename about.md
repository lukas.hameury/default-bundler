---
layout: page
title: À propos
permalink: /about/
---

Ceci est une démonstration de la fonctionnalité des sites.
Grâce à cela, un fichier markdown peut être affiché directement sur une page web, `formaté` comme désiré.

You can find the source code for the Jekyll new theme at:
{% include icon-github.html username="jglovier" %} /
[jekyll-new](https://github.com/jglovier/jekyll-new)

You can find the source code for Jekyll at
{% include icon-github.html username="jekyll" %} /
[jekyll](https://github.com/jekyll/jekyll)
